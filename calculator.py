from art import logo

def add(n1,n2):
    return n1 + n2

def subtract(n1,n2):
    return n1 - n2

def multiply(n1,n2):
    return n1 * n2

def divide(n1,n2):
    return n1 / n2

operations = {
    "+" : add,
    "-" : subtract,
    "*" : multiply,
    "/" : divide
}

def calculator():
    print(logo)
    continue_calculation = True
    n1 = float(input("Enter a number:- "))
    for symbol in operations:
        print(symbol)
    while continue_calculation:
        operation  = input("Enter an operation to perform: ")
        n2 = float(input("Enter another number:- "))

        calculation_result = operations[operation](n1,n2) 

        print(f"{n1} {operation} {n2} = {calculation_result}")

        if input("Type 'Y' to perform another calculation: ").upper() == "Y":
            n1 = calculation_result
        else:
            continue_calculation = False
            calculator()        

calculator()
